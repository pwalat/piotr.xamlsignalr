﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piotr.XamlSignalR.App.WindowsPhone.Data
{
    public interface IConnectedDataProvider
    {
        Task StartAsync();
        void Stop();
    }
}
