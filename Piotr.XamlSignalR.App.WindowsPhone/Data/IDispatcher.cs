﻿using System;
using GalaSoft.MvvmLight.Threading;

namespace Piotr.XamlSignalR.App.WindowsPhone.Data
{
    public interface IDispatcher
    {
        void Dispatch(Action action);
    }

    public class PhoneDispatcher : IDispatcher
    {
        public PhoneDispatcher()
        {
            DispatcherHelper.Initialize();
        }

        public void Dispatch(Action action)
        {
            DispatcherHelper.RunAsync(action);
        }
    }
}