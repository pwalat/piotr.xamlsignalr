﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Piotr.XamlSignalR.App.WindowsPhone.Data;
using Piotr.XamlSignalR.App.WindowsPhone.ViewModel;

namespace Piotr.XamlSignalR.App.WindowsPhone.Messaging
{
    public class ConnectionStateChangedMessage
    {
        public ConnectionState OldState { get; set; }
        public ConnectionState NewState { get; set; }
    }

    public class QuoteUpdatedMessage
    {
        public Quote Quote { get; set; }
    }

    public static class ConnectionStateConverter
    {
        public static ConnectionState ToConnectionState(Microsoft.AspNet.SignalR
            .Client.ConnectionState connectionState)
        {
            return (ConnectionState)Enum.Parse(typeof(ConnectionState),
                connectionState.ToString());
        }
    }
}
