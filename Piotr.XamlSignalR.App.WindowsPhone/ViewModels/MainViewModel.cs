using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using Piotr.XamlSignalR.App.WindowsPhone.Data;
using Piotr.XamlSignalR.App.WindowsPhone.Messaging;

namespace Piotr.XamlSignalR.App.WindowsPhone.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private ConnectionState _connectionState;
        public ConnectionState ConnectionState
        {
            get { return _connectionState; }
            set
            {
                if (_connectionState == value) return;
                _connectionState = value;
                RaisePropertyChanged("ConnectionState");
                RaisePropertyChanged("IsConnected");
            }
        }

        public bool IsConnected
        {
            get { return ConnectionState == ConnectionState.Connected; }
        }

        public MainViewModel()
        {
            Items = new ObservableCollection<Quote>();

            MessengerInstance.Register<QuoteUpdatedMessage>(this, UpdateQuoteHandler);
            MessengerInstance.Register<ConnectionStateChangedMessage>(this,
                                                                      ConnectionStateChangedHandler);
        }

        private void ConnectionStateChangedHandler(ConnectionStateChangedMessage msg)
        {
            ConnectionState = msg.NewState;
        }

        private void UpdateQuoteHandler(QuoteUpdatedMessage msg)
        {
            var quote = msg.Quote;
            var match = Items.FirstOrDefault(q => q.Name == quote.Name);
            if (match != null)
            {
                match.Price = quote.Price;
                match.PriceChange = quote.PriceChange;
            }
            else
            {
                Items.Add(quote);
            }
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<Quote> Items { get; private set; }

        public override void Cleanup()
        {
            MessengerInstance.Unregister(this);
            base.Cleanup();
        }
    }
}