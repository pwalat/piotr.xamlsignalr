﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Piotr.XamlSignalR.Service.Data;

namespace Piotr.XamlSignalR.Service.Hubs
{
    [HubName("Quote")]
    public class QuoteHub : Hub
    {
        public void UpdateQuote(Quote quote)
        {
            Clients.All.updateQuote(quote);
        }
    }
}